SRC = ./src/main
RES = ./src/resources
BIN = ./bin
RUN = ./run
LIB = ./lib
JAVAC = javac -source 1.4 -target 1.4 -classpath $(LIB)/Kindlet-2.2.jar -d $(BIN) -sourcepath $(SRC)
sources := $(shell find . -type f -name '*.java')
classes := $(patsubst $(SRC)/%.java,$(BIN)/%.class, $(sources))

# default target builds the java code
all : compile

compile : $(classes)

$(BIN)/%.class : $(SRC)/%.java
	$(JAVAC) $<

# build, package and sign for kindle
package : $(classes)
	cp -a $(RES)/META-INF $(BIN)/
	cp $(RES)/KindleLauncher.jpg $(BIN)/
	cd $(BIN) && jar cvmf META-INF/MANIFEST.MF ../info.jar *
	jarsigner -sigalg SHA1withDSA -keystore developer.keystore -storepass password info.jar dktest
	jarsigner -sigalg SHA1withDSA -keystore developer.keystore -storepass password info.jar ditest
	jarsigner -sigalg SHA1withDSA -keystore developer.keystore -storepass password info.jar dntest

# run locally as a Swing application
run : $(classes)
	mkdir $(RUN)/
	cp -a $(BIN)/* $(RUN)/
	cp -a $(LIB) $(RUN)/
	cd $(RUN) && java -cp .:$(LIB)/Kindlet-2.2.jar uk.co.greenlightapps.info.LocalMain
	rm -rf $(RUN)

clean :
	rm -rf $(BIN)/* info.jar
