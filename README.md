# About this thing

TODO

## Pre-requisites

### Dev platform

I use a Mac, and tested this with the Oracle "Java SE Development Kit 8u202", [downloaded directly from Oracle](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html):

```
$ java -version
java version "1.8.0_202"
Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)

```

### Kindle Touch Preparation

* I have a Kindle Touch (model D01200), also known as "K5", running firmware 5.3.7.3 (2715280002), which I factory-reset
* Your Kindle will need to be registered (logged in with an Amazon account) in order for Kindlets to access the network
  * To register with an account with 2FA, enter your password plus the OTP in the password box
* Jailbreak with the "K5 Jailbreak", discussed in [this wiki page](https://wiki.mobileread.com/wiki/Kindle_Touch_Hacking#Jailbreak), [this forum thread](https://www.mobileread.com/forums/showthread.php?t=186645) using version 1.16.N-r19326 of the "K5 Jailbreak" in [this Snapshots thread](https://www.mobileread.com/forums/showthread.php?t=225030).
* Install KUAL following [the instructions in this Wiki page](https://wiki.mobileread.com/wiki/Kindle_Touch_Hacking#Kindle_Unified_Applications_Launcher), version v2.7.32-g74c69f9-20240104
* Then install MRPI (the MR Package Installer)
* Install [USBNetwork Hack](https://www.mobileread.com/forums/showthread.php?t=186645) (version 0.22.N-r19297) by putting the USB Networking BIN into the `mrpackages` folder, so it can be installed using KUAL
* Set the correct timezone, [following these instructions](https://wiki.mobileread.com/wiki/Kindle_Touch_Hacking#Setting_the_time_zone). You may need to change the link to use "http" (not "https") if you're using `wget` to download the tz file on Kindle itself

With that done, you can SSH over USB... and then copy over your public key to the `authorized_keys` file, and use KUAL to enable:
* SSH over WIFI
* SSH on boot
* OpenSSH instead of Dropbear

Also useful is [kual-helper](https://www.mobileread.com/forums/showthread.php?t=203326) which provides a "Prevent Screensaver" button in KUAL. I used version 0.5.N-r18980. Alternatively, enter `~ds` into the search bar to disable the screensaver.

### Hacking the Java security policy

Unfortunately, the Kindle Touch is stuck with Java 1.4, which does not support modern versions of TLS. Our Octopus API _requires_ modern TLS, so we can't get Octopus Agile rates with a traditional network request. We need to be creative.

The system does have `curl` installed, which _can_ use TLSv1.2, so we'll use that via Java's `Runtime.exec` call. That call needs extra permissions that Kindlets wouldn't normally get, so we need to hack the Java security policy on the device.

SSH into the device, and then:
* `mntroot rw` to re-mount the root filesystem as read/write
* `vi /usr/java/lib/security/java.policy` to edit the policy
* replace the contents of the file with this:

```
grant {
        permission java.security.AllPermission;
};
```

Doing that is probably a bad idea.

## Build preparation

For Kindlets to run, they must have a valid signature. Traditionally, developers would make their own signing keys, which meant that _users_ would need to insert the developer's keys into the Java keystore on the Kindle.

But now, all devs have agreed to use a common set of "test" keys, which (usefully) are installed as part of the jailbreak process.

[This wiki page](https://wiki.mobileread.com/wiki/Kindlet_Developer_HowTo) is useful for Kindlet development, but ignore the part about generating your own keys and merging keystores - it's not needed.

Instead, download the latest version of the [Merged Developer Keystore](https://www.mobileread.com/forums/showthread.php?t=152294).

## Building, running locally, and packaging

To run locally as a Java Swing application:

```
build run
```

To package and sign for Kindle:

```
build package
```

To copy the signed jar to the Kindle:

```
scp info.jar root@kindle://mnt/us/documents/info.azw2
```

## Troubleshooting

### Complaints about "untrusted developer"

Check that you are signing with the shared "test" keys, and the signature algorithm is SHA1withDSA.

### Cannot find main class

First (obviously) check that the class name is correct in the manifest.

Then check that your classes are version 48 (JDK 1.4). I had problems with version 52 (Java SE 8).


## References

["Kindle Touch Hacking" wiki page](https://wiki.mobileread.com/wiki/Kindle_Touch_Hacking)

## Random dev notes

Kindle Touch appears to support 16 greyscales.

Missing symbols from the Kindle JRE 1.4:

* `java.awt.Graphics2D`
* the `X` format symbol in `SimpleDateFormat`
* `FontMetrics.getStringBounds(String, Graphics)`
* `Timer(String)` constructor
