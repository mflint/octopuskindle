package uk.co.greenlightapps.info.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RuntimeRunner {
	public static class RuntimeResult {
		public int exitValue;
		public String output;
	
		public RuntimeResult(int exitValue, String output) {
			this.exitValue = exitValue;
			this.output = output;
		}
	}

	public RuntimeResult run(String[] command) throws Exception {
		final Process process = Runtime.getRuntime().exec(command);

		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuffer buffer = new StringBuffer();

		String line;
		while(null != (line = reader.readLine())) {
			buffer.append(line);
		}

		reader.close();
		process.waitFor();

		return new RuntimeResult(process.exitValue(), buffer.toString());
	}
}
