package uk.co.greenlightapps.info.util;

import uk.co.greenlightapps.info.util.RuntimeRunner.RuntimeResult;

public class KindleScreenSaver {
	private boolean screenSaverEnabledOnLaunch;

	public KindleScreenSaver() {
		try {
			RuntimeResult state = new RuntimeRunner().run(new String[] {
				"lipc-get-prop",
				"-e",
				"com.lab126.powerd",
				"preventScreenSaver"
			});
			if (state.output.equals("0")) {
				this.screenSaverEnabledOnLaunch = true;
				new RuntimeRunner().run(new String[] {
					"lipc-set-prop", 
					"com.lab126.powerd", 
					"preventScreenSaver", 
					"1"
				});
			} else {
				this.screenSaverEnabledOnLaunch = false;
			}
		} catch (Exception e) {}
	}

	public void restore() {
		if (this.screenSaverEnabledOnLaunch) {
			try {
				new RuntimeRunner().run(new String[] {
					"lipc-set-prop",
					 "com.lab126.powerd", 
					"preventScreenSaver", 
					"0"
				});
			} catch (Exception e) {}
		}
	}
}
