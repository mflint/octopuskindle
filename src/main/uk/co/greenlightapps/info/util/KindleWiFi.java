package uk.co.greenlightapps.info.util;

public class KindleWiFi {
	private static class NoWifiConnectionException extends Exception {}

	public static KindleWiFi SHARED = new KindleWiFi();

	private boolean canControlWiFi;

	public KindleWiFi() {
		try {
			new RuntimeRunner().run(new String[] {"lipc-get-prop", "-e", "com.lab126.cmd", "wirelessEnable"});
			this.canControlWiFi = true;
		} catch (Exception e) {
			this.canControlWiFi = false;
		}
	}

	public void turnOn() {
		if (!this.canControlWiFi) {
			return;
		}

		try {
			new RuntimeRunner().run(new String[] {"lipc-set-prop", "com.lab126.cmd", "wirelessEnable", "1"});
		} catch (Exception e) {
		}
	}

	public void turnOff() {
		if (!this.canControlWiFi) {
			return;
		}

		try {
			new RuntimeRunner().run(new String[] {"lipc-set-prop", "com.lab126.cmd", "wirelessEnable", "0"});
		} catch (Exception e) {
		}
	}

	public boolean isConnected() {
		// CONNECTED: connected to a network
		// PENDING: WiFi is on, but not connected to a network
		// NA: WiFi is turned off
		try {
			return new RuntimeRunner().run(new String[] {"lipc-get-prop", "com.lab126.wifid", "cmState"}).output.equals("CONNECTED");
		} catch (Exception e) {
			return true;
		}
	}

	// Enable WiFi and wait for a connection
	// Times out after 15 seconds
	// Warning: this blocks the current thread
	public void establishConnection() throws Exception {
		if (isConnected()) {
			return;
		}

		turnOn();

		int count = 0;
		while (!isConnected()) {
			Thread.sleep(1000);
			count += 1;
			if (count == 15) {
				throw new NoWifiConnectionException();
			}
		}
	}
}
