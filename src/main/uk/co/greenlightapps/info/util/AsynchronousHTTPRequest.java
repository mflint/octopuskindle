package uk.co.greenlightapps.info.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import uk.co.greenlightapps.info.util.RuntimeRunner.RuntimeResult;

public final class AsynchronousHTTPRequest {
	private static class NetworkException extends Exception {}

	private static final AsynchronousHTTPRequest INSTANCE = new AsynchronousHTTPRequest();
	private final AsynchronousHTTPRequest.BackgroundRequestRunnable requestRunnable = new AsynchronousHTTPRequest.BackgroundRequestRunnable();

	private AsynchronousHTTPRequest() {
		(new Thread(this.requestRunnable)).start();
	}

	public static AsynchronousHTTPRequest getInstance() {
		return INSTANCE;
	}

	public void addRequest(String url, AsynchronousHTTPRequest.IAsynchronousHTTPResponse callback) {
		this.requestRunnable.addRequest(url, callback);
	}

	public void kill() {
		this.requestRunnable.kill();
	}

	private static final class BackgroundRequestRunnable implements Runnable {
		private final List requests = new ArrayList();
		private boolean running = true;

		public BackgroundRequestRunnable() {
		}

		public void run() {
			while(this.running) {
				AsynchronousHTTPRequest.RequestObject request = null;
				synchronized(this.requests) {
					if (this.requests.size() > 0) {
					request = (AsynchronousHTTPRequest.RequestObject)this.requests.remove(0);
					}
				}

				if (null != request) {
					this.sendRequest(request);
				} else if (this.running) {
					try {
						synchronized(this) {
							this.wait();
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				}
			}
		}

		private void sendRequest(AsynchronousHTTPRequest.RequestObject request) {
			try {
				String[] command = { "curl", request.url };
				RuntimeResult result = new RuntimeRunner().run(command);

				// curl returns 0 on success
				if (result.exitValue == 0 && result.output.length() > 0) {
					request.callback.onSuccess(result.output);
				} else {
					request.callback.onFailure(new NetworkException());
				}
			} catch (Exception e) {
				request.callback.onFailure(e);
			}
		}

		public void addRequest(String url, AsynchronousHTTPRequest.IAsynchronousHTTPResponse callback) {
			synchronized(this.requests) {
				this.requests.add(new AsynchronousHTTPRequest.RequestObject(url, callback));
			}

			synchronized(this) {
				this.notifyAll();
			}
		}

		public void kill() {
			this.running = false;
			synchronized(this) {
				this.notifyAll();
			}
		}
	}

	private static final class RequestObject {
		private final String url;
		private final AsynchronousHTTPRequest.IAsynchronousHTTPResponse callback;

		public RequestObject(String url, AsynchronousHTTPRequest.IAsynchronousHTTPResponse callback) {
			this.url = url;
			this.callback = callback;
		}
	}

	public interface IAsynchronousHTTPResponse {
		void onSuccess(String body);

		void onFailure(Exception e);
	}
}
