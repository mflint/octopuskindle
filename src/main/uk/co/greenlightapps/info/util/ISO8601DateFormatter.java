package uk.co.greenlightapps.info.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

final public class ISO8601DateFormatter {
	/*
	* see https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
	* X: parses ISO8601 timezones. Works locally. On Kindle, "illegal pattern chatacter"
	* Z: RFC 822 time zone	-0800 - works on Kindle
	*/
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

	public static Date parse(String dateString) {
		if (dateString.endsWith("Z")) {
			dateString = dateString.substring(0, dateString.length() - 1).concat("+0000");
		}
		
		try {
			return dateFormat.parse(dateString);
		} catch(Exception e) {
			return null;
		}
	}
}
