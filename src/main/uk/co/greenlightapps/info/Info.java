package uk.co.greenlightapps.info;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KRepaintManager;
import com.amazon.kindle.kindlet.ui.OrientationController;

import uk.co.greenlightapps.info.octopus.*;
import uk.co.greenlightapps.info.ui.IScreenRefresh;
import uk.co.greenlightapps.info.ui.IView;

import java.awt.*;

public class Info implements IScreenRefresh
{	
	private KRepaintManager repaintManager = KRepaintManager.getInstance();
	private KindletContext context;
	private Container rootContainer;
	private IView view;

	private int refreshCount = 0;
	
	public void init(KindletContext kindletContext) {
		this.context = kindletContext;

		OrientationController orientationController = (OrientationController)kindletContext.getService(OrientationController.class);
		orientationController.lockOrientation(3);
	}

	public void start() {
		this.rootContainer = this.context.getRootContainer();
		this.rootContainer.setLayout(new BorderLayout());

		this.setView(new OctopusView(this));
	}

	private void setView(IView view) {
		this.rootContainer.removeAll();

		if (this.view != null) {
			this.view.stop();
			this.view.destroy();
		}

		view.start();
		rootContainer.add(view.view(), BorderLayout.CENTER);

		this.view = view;
	}

	public void doRefresh() {
		this.rootContainer.invalidate();
		this.rootContainer.validate();
		this.rootContainer.repaint();

		this.refreshCount += 1;
		if (null != repaintManager && this.refreshCount == 4) {
			repaintManager.repaint(this.rootContainer, true);
			this.refreshCount = 0;
		}
	}
	
	public void stop() {
		if (this.view != null) {
			this.view.stop();
		}
	}
	
	public void destroy() {
		if (this.view != null) {
			this.view.destroy();
		}
	}
}
