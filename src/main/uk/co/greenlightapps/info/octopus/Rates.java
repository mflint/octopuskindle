package uk.co.greenlightapps.info.octopus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

import uk.co.greenlightapps.info.util.AsynchronousHTTPRequest;
import uk.co.greenlightapps.info.util.ISO8601DateFormatter;
import uk.co.greenlightapps.info.util.KindleWiFi;

public class Rates {
    public interface IRatesFetchListener {
		void onRatesRequesting();
        void onRatesChanged();
        void onRatesUpdateFailure(Exception e);
    }

	public static class Rate {
		public Number valueIncVat;
		public Date validFrom;
		public Date validTo;
		public String raw;

		Rate(JSONObject jsonObject) throws Exception {
			Double valueIncVat = (Double)jsonObject.get("value_inc_vat");

			String validFromString = (String)jsonObject.get("valid_from");
			String validToString = (String)jsonObject.get("valid_to");
			this.validFrom = ISO8601DateFormatter.parse(validFromString);
			this.validTo = ISO8601DateFormatter.parse(validToString);
			this.raw = validFromString + " - " + validToString;

			if (valueIncVat == null || this.validFrom == null || this.validTo == null) {
				throw new RateParseException("rate parse exception");
			}

			this.valueIncVat = new BigDecimal(valueIncVat.doubleValue())
				.setScale(2, BigDecimal.ROUND_CEILING);
		}
	}

	public static class RateParseException extends Exception {
		String reason;

		RateParseException(String reason) {
			this.reason = reason;
		}

		public String toString() {
			return this.reason;
		}
	}

	private Rate[] importRates = new Rate[0];
	private Rate[] exportRates = new Rate[0];

	private IRatesFetchListener listener;

	private Product importProduct = Product.IMPORT;
	private Product exportProduct = Product.EXPORT;

	public Rates(IRatesFetchListener listener) {
		this.listener = listener;
	}

	public void start() {
		new Thread() {
			public void run() {
				Rates.this.backgroundFetch();
			}
		}.start();
	}

	// Warning: this blocks the current thread
	private void backgroundFetch() {
		boolean shouldFetchImport = this.shouldFetchImport();
		boolean shouldFetchExport = this.shouldFetchExport();

		if (!shouldFetchImport && !shouldFetchExport) {
			return;
		}

		if (this.listener != null) {
			this.listener.onRatesRequesting();
		}

		try {
			KindleWiFi.SHARED.establishConnection();

			if (shouldFetchImport()) {
				this.fetchImport();
			} else if (shouldFetchExport()) {
				this.fetchExport(new Rate[0]);
			}
		} catch (Exception e) {
			this.handleFailure(e);
		}
	}

	public void stop() {

	}

	public Rate[] getTodayImport() {
		Rate[] rates = this.importRates;
		Date startOfToday = this.startOfToday();
		Date startOfTomorrow = this.startOfTomorrow();

		ArrayList result = new ArrayList();

		for (int i = 0; i < rates.length; i++) {
			Rate rate = rates[i];
			if (rate.validFrom.compareTo(startOfToday) >= 0 && rate.validFrom.compareTo(startOfTomorrow) < 0) {
				result.add(rate);
			}
		}

		return (Rate[])result.toArray(new Rate[result.size()]);
	}

	public Rate[] getTomorrowImport() {
		Rate[] rates = this.importRates;
		Date startOfTomorrow = this.startOfTomorrow();

		ArrayList result = new ArrayList();

		for (int i = 0; i < rates.length; i++) {
			Rate rate = rates[i];
			if (rate.validFrom.compareTo(startOfTomorrow) >= 0) {
				result.add(rate);
			}
		}

		return (Rate[])result.toArray(new Rate[result.size()]);
	}

	public Rate[] getTodayExport() {
		Rate[] rates = this.exportRates;
		Date startOfToday = this.startOfToday();
		Date startOfTomorrow = this.startOfTomorrow();

		ArrayList result = new ArrayList();

		for (int i = 0; i < rates.length; i++) {
			Rate rate = rates[i];
			if (rate.validFrom.compareTo(startOfToday) >= 0 && rate.validFrom.compareTo(startOfTomorrow) < 0) {
				result.add(rate);
			}
		}

		return (Rate[])result.toArray(new Rate[result.size()]);
	}

	public Rate[] getTomorrowExport() {
		Rate[] rates = this.exportRates;
		Date startOfTomorrow = this.startOfTomorrow();

		ArrayList result = new ArrayList();

		for (int i = 0; i < rates.length; i++) {
			Rate rate = rates[i];
			if (rate.validFrom.compareTo(startOfTomorrow) >= 0) {
				result.add(rate);
			}
		}

		return (Rate[])result.toArray(new Rate[result.size()]);
	}

	private Date startOfToday() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	private Date startOfTomorrow() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}

	private boolean shouldFetchImport() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		boolean isAfter4 = calendar.get(Calendar.HOUR_OF_DAY) >= 16;

		Rate[] todayRates = this.getTodayImport();
		Rate[] tomorrowRates = this.getTomorrowImport();

		return importProduct != null &&
			(todayRates.length == 0 || (tomorrowRates.length == 0 && isAfter4));
	}

	private boolean shouldFetchExport() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		boolean isAfter4 = calendar.get(Calendar.HOUR_OF_DAY) >= 16;

		Rate[] todayRates = this.getTodayExport();
		Rate[] tomorrowRates = this.getTomorrowExport();

		return exportProduct != null &&
			(todayRates.length == 0 || (tomorrowRates.length == 0 && isAfter4));
	}

	private void fetchImport() {
		AsynchronousHTTPRequest.getInstance().addRequest("https://api.octopus.energy/v1/products/" + this.importProduct.productCode + "/electricity-tariffs/" + this.importProduct.tariff + "/standard-unit-rates/", new AsynchronousHTTPRequest.IAsynchronousHTTPResponse() {
			public void onSuccess(String body) {
				try {
					Rate[] importRates = Rates.this.parse(body);
					if (Rates.this.shouldFetchExport()) {
						Rates.this.fetchExport(importRates);
					} else {
						Rates.this.handleSuccess(importRates, new Rate[0]);
					}
				} catch (Exception e) {
					Rates.this.handleFailure(e);
				}
			}

			public void onFailure(Exception e) {
				Rates.this.handleFailure(e);
			}
		});
	}

	private void fetchExport(final Rate[] importRates) {
		AsynchronousHTTPRequest.getInstance().addRequest("https://api.octopus.energy/v1/products/" + this.exportProduct.productCode + "/electricity-tariffs/" + this.exportProduct.tariff + "/standard-unit-rates/", new AsynchronousHTTPRequest.IAsynchronousHTTPResponse() {
			public void onSuccess(String body) {
				try {
					Rate[] exportRates = Rates.this.parse(body);
					Rates.this.handleSuccess(importRates, exportRates);
				} catch (Exception e) {
					Rates.this.handleFailure(e);
				}
			}

			public void onFailure(Exception e) {
				Rates.this.handleFailure(e);
			}
		});
	}

	private void handleFailure(Exception e) {
		if (this.listener != null) {
			this.listener.onRatesUpdateFailure(e);
		}

		KindleWiFi.SHARED.turnOff();
	}

	private void handleSuccess(Rate[] importRates, Rate[] exportRates) {
		this.importRates = importRates;
		this.exportRates = exportRates;

		if (this.listener != null) {
			this.listener.onRatesChanged();
		}

		KindleWiFi.SHARED.turnOff();
	}

	private Rate[] parse(String jsonString) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject)parser.parse(jsonString);
		JSONArray results = (JSONArray)obj.get("results");
		ArrayList rates = new ArrayList();

		for (int i = 0; i < results.size(); i++) {
			Rate rate = new Rate((JSONObject)results.get(i));
			rates.add(rate);
		}
		
		Comparator comparator = new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Rate)o1).validFrom.compareTo(((Rate)o2).validFrom);
			}
		};
		
		Collections.sort(rates, comparator);
		return (Rate[])rates.toArray(new Rate[rates.size()]);
	}
}
