package uk.co.greenlightapps.info.octopus;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class OctopusButtons extends JPanel {
	public interface OctopusButtonsListener {
		void todaySelected();
		void tomorrowSelected();
		void importSelected();
		void exportSelected();
	}

	private OctopusButtonsListener listener;

	private JRadioButton today;
	private JRadioButton tomorrow;

	private JRadioButton importRates;
	private JRadioButton exportRates;

	public OctopusButtons(OctopusButtonsListener listener) {
		super();
		this.setLayout(new FlowLayout());

		this.listener = listener;

		ButtonGroup dayButtonGroup = new ButtonGroup();

		this.today = new JRadioButton("Today", true);
		this.tomorrow = new JRadioButton("Tomorrow");

		dayButtonGroup.add(today);
		dayButtonGroup.add(tomorrow);

		today.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OctopusButtons.this.listener.todaySelected();
			}
		});

		tomorrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OctopusButtons.this.listener.tomorrowSelected();
			}
		});

		ButtonGroup importExportButtonGroup = new ButtonGroup();

		this.importRates = new JRadioButton("Import", true);
		this.exportRates = new JRadioButton("Export");

		importExportButtonGroup.add(importRates);
		importExportButtonGroup.add(exportRates);

		importRates.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OctopusButtons.this.listener.importSelected();
			}
		});

		exportRates.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OctopusButtons.this.listener.exportSelected();
			}
		});
	}

	void setState(boolean showTomorrow, boolean showImport, boolean showExport) {
		this.removeAll();

		if (showTomorrow) {
			this.add(today);
			this.add(tomorrow);
		}

		if (showTomorrow && showImport && showExport) {
			this.add(Box.createHorizontalStrut(75));
		}

		if (showImport && showExport) {
			this.add(importRates);
			this.add(exportRates);
		}
	}
}
