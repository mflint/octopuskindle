package uk.co.greenlightapps.info.octopus;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComponent;

import uk.co.greenlightapps.info.octopus.Rates.Rate;
import uk.co.greenlightapps.info.ui.GraphicsDrawString;
import uk.co.greenlightapps.info.ui.Greyscale;
import uk.co.greenlightapps.info.ui.GraphicsDrawString.Anchor;

class OctopusGraph extends JComponent {
	public interface OctopusGraphListener {
		void rateSelected(Rate rate);
	}

private OctopusGraphListener listener;

	private Rate[] rates;
	private double rateMin;
	private double rateMax;

	private int xOrigin;
	private int graphWidth;
	private int graphHeight;
	private double yScale;
	private int yOrigin;
	private double xScale;

	private Rate highlightedRate;

	OctopusGraph(OctopusGraphListener listener) {
		this.listener = listener;

		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				OctopusGraph.this.onTap(e.getX(), e.getY());
			}
		});
	}

	private void onTap(int x, int y) {
		if (x >= this.xOrigin &&
			x < this.xOrigin + this.graphWidth) {
				int rateIndex = (int)((x - xOrigin) / this.xScale);
				if (rateIndex < this.rates.length) {
					this.listener.rateSelected(this.rates[rateIndex]);
				}
			}
	}

	public void setRates(Rate[] rates, Rate highlightedRate) {
		if (rates == null) {
			return;
		}

		this.highlightedRate = highlightedRate;

		rateMin = 0;

		if (rates.length > 0) {
			rateMax = rates[0].valueIncVat.doubleValue();
		} else {
			rateMax = 0;
		}

		for (int i = 0; i < rates.length; i++) {
			double val = rates[i].valueIncVat.doubleValue();
			if (val > rateMax) {
				rateMax = val;
			}
			if (val < rateMin) {
				rateMin = val;
			}
		}

		rateMax = Math.ceil(rateMax);
		if (rateMin < 0) {
			rateMin = Math.floor(rateMin);
		}

		this.rates = rates;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		GraphicsDrawString gds = new GraphicsDrawString(g);

		if (this.rates.length == 0) {
			g.drawString("<<no rates>>", 50, 100);
			return;
		}

		Dimension size = this.getSize();

		int rateCount = rates.length;

		this.xOrigin = 50;
		this.graphWidth = (int)size.getWidth() - xOrigin;
		this.graphHeight = (int)size.getHeight() - 30;
		this.yScale = graphHeight / (rateMax - rateMin);
		this.yOrigin = (int)(graphHeight * rateMax / (rateMax - rateMin));
		this.xScale = graphWidth / rateCount;

		// horizontal bars
		for (int i = (int)(Math.ceil(rateMin / 2) * 2); i < (int)(Math.floor(rateMax / 2) * 2); i+=4) {
			g.setColor(Greyscale.value[12]);
			g.fillRect(xOrigin, (int)(yOrigin - ((i + 2) * yScale)), graphWidth, (int)(yScale * 2));
		}

		g.setColor(Greyscale.value[2]);

		// polyline
		int[] polyX = new int[(rateCount * 2) + 2];
		int[] polyY = new int[(rateCount * 2) + 2];
		polyX[0] = xOrigin;
		polyY[0] = yOrigin;
		for (int i = 0; i < rateCount; i++) {
			double rate = rates[i].valueIncVat.doubleValue();
			polyX[(i*2)+1] = (int)(xOrigin + (i * xScale));
			polyY[(i*2)+1] = (int)(yOrigin - (rate * yScale));
			polyX[(i*2)+2] = (int)(xOrigin + ((i+1) * xScale));
			polyY[(i*2)+2] = (int)(yOrigin - (rate * yScale));
		}
		polyX[(rateCount * 2) + 1] = xOrigin + (int)(rateCount * xScale);
		polyY[(rateCount * 2) + 1] = yOrigin;

		// polygon fill
		Polygon p = new Polygon();
		p.addPoint(xOrigin, yOrigin);
		for (int i = 0; i < polyX.length; i++) {
			p.addPoint(polyX[i], polyY[i]);
		}
		g.setColor(Greyscale.value[7]);
		g.fillPolygon(p);

		// vertical lines
		g.setColor(Greyscale.value[10]);

		for (int i = 0; i <= rateCount; i++) {
			int x = (int)(xOrigin + (i * xScale));
			g.drawLine(x, 0, x, graphHeight);
		}

		// highlight current rate
		for (int i = 0; i < rateCount; i++) {
			Rate rate = rates[i];
			if (rate == highlightedRate) {
				int barHeight = (int)(rate.valueIncVat.doubleValue() * yScale);
				int left = (int)(xOrigin + (i * xScale));
				g.setColor(Greyscale.value[2]);
				if (barHeight > 0) {
					g.fillRect(left, yOrigin - barHeight, (int)xScale, barHeight);
				} else {
					g.fillRect(left, yOrigin, (int)xScale, -barHeight);
				}
			}
		}

		// polygon lines
		g.setColor(Greyscale.value[0]);
		for (int i = 0; i < polyX.length - 1; i++) {
			// vertical
			if (polyY[i] < polyY[i+1]) {
				g.fillRect(polyX[i] - 1, polyY[i], 2, polyY[i+1] - polyY[i]);
			} else {
				g.fillRect(polyX[i] - 1, polyY[i+1], 2, polyY[i] - polyY[i+1]);
			}
			// horizontal
			g.fillRect(polyX[i] - 1, polyY[i] - 1, polyX[i + 1] - polyX[i] + 2, 2);
		}
		
		// price labels, y axis
		g.setColor(Greyscale.value[0]);

		for (int i = (int)(Math.ceil(rateMin / 2) * 2); i < (int)(Math.ceil(rateMax / 2) * 2); i+=2) {
			gds.drawString(new Integer(i).toString(), xOrigin, (int)(yOrigin - (i * yScale)), Anchor.TRAILING);
		}

		// time labels, x axis
		DateFormat timeFormat = new SimpleDateFormat("HH");
		for (int i = 2; i < rateCount; i+=2) {
			String s = timeFormat.format(rates[i].validFrom);
			gds.drawString(s, (int)(xOrigin + (i * xScale)), graphHeight, Anchor.TOP);
		}
		// final one
		String finalTime = timeFormat.format(rates[rates.length - 1].validTo);
		gds.drawString(finalTime, (int)(xOrigin + (rates.length * xScale)), graphHeight, Anchor.TOP);
	}
}
