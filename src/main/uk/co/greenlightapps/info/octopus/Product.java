package uk.co.greenlightapps.info.octopus;

public class Product {
	public String productCode;
	public String tariff;

	public static Product IMPORT = new Product("AGILE-23-12-06", "E-1R-AGILE-23-12-06-B");
	public static Product EXPORT = new Product("AGILE-OUTGOING-19-05-13", "E-1R-AGILE-OUTGOING-19-05-13-B");

	public Product(String productCode, String tariff) {
		this.productCode = productCode;
		this.tariff = tariff;
	}
}
