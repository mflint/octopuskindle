package uk.co.greenlightapps.info.octopus;

import java.awt.BorderLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import uk.co.greenlightapps.info.octopus.OctopusButtons.OctopusButtonsListener;
import uk.co.greenlightapps.info.octopus.OctopusGraph.OctopusGraphListener;
import uk.co.greenlightapps.info.octopus.Rates.Rate;
import uk.co.greenlightapps.info.ui.IScreenRefresh;
import uk.co.greenlightapps.info.ui.IView;
import uk.co.greenlightapps.info.util.KindleWiFi;

public class OctopusView implements IView {
	private static DateFormat hourMinuteDateFormat = new SimpleDateFormat("HH:mm");
	
	// was the WiFi enabled when kindlet was launched?
	private boolean wiFiConnectedWhenLaunched;
	private IScreenRefresh screenRefresh;
	private JPanel panel;

	private OctopusTitleView titleView;
	private OctopusButtons octopusButtons;
	private OctopusGraph octopusGraph;

	private Timer selectedRateResetTimer;
	private Timer automaticRateSelectionTimer;

	// source data
	private Rates rates;
	private boolean showToday = true;
	private boolean showImportRates = true;
	private boolean requesting = false;
	private String error = null;
	private Date selectedRateStartMoment = null; // explicitly selected rate

	// calculated values
	private Rate[] displayRates;
	private Rate highlightedRate;
	private String title;
	private boolean canShowTomorrow;
	private boolean canShowImport;
	private boolean canShowExport;

	public OctopusView(IScreenRefresh screenRefresh) {
		this.wiFiConnectedWhenLaunched = KindleWiFi.SHARED.isConnected();

		this.rates = new Rates(new Rates.IRatesFetchListener() {
			public void onRatesRequesting() {
				OctopusView.this.setRequesting();	
			}

			public void onRatesChanged() {
				OctopusView.this.handleNewRates();
			}

			public void onRatesUpdateFailure(Exception e) {
				OctopusView.this.setError(e.toString());
			}
		});
		
		this.screenRefresh = screenRefresh;
		this.panel = new JPanel();
		this.panel.setLayout(new BorderLayout());

		this.titleView = new OctopusTitleView();

		this.octopusButtons = new OctopusButtons(new OctopusButtonsListener() {
			public void todaySelected() {
				OctopusView.this.showToday();
			}

			public void tomorrowSelected() {
				OctopusView.this.showTomorrow();
			}

			public void importSelected() {
				OctopusView.this.showImportRates();
			}

			public void exportSelected() {
				OctopusView.this.showExportRates();
			}
		});

		this.octopusGraph = new OctopusGraph(new OctopusGraphListener() {
			public void rateSelected(Rate rate) {
				OctopusView.this.selectRate(rate);
			}
		});
	}

	// state changing functions

	private void showToday() {
		this.showToday = true;
		this.selectedRateStartMoment = null;
		this.updateUI(false);
	}

	private void showTomorrow() {
		this.showToday = false;
		this.selectedRateStartMoment = null;
		this.updateUI(false);
	}

	private void showImportRates() {
		this.showImportRates = true;
		this.selectedRateStartMoment = null;
		this.updateUI(false);
	}

	private void showExportRates() {
		this.showImportRates = false;
		this.selectedRateStartMoment = null;
		this.updateUI(false);
	}

	private void selectRate(Rate rate) {
		this.selectedRateStartMoment = rate != null ? rate.validFrom : null;
		this.updateUI(false);
		this.startSelectedRateResetTimer();
	}

	private void updateUI(final boolean rebuild) {
		Rate[] todayImport = this.rates.getTodayImport();
		Rate[] todayExport = this.rates.getTodayExport();

		Rate[] displayRates = null;
		String title = "";
		Rate highlightedRate = null;

		this.canShowImport = todayImport.length > 0;
		this.canShowExport = todayExport.length > 0;

		if (this.canShowImport && !this.canShowExport) {
			this.showImportRates = true;
		} else if (!this.canShowImport && this.canShowExport) {
			this.showImportRates = false;
		}

		// get the set of rates that we need to display in the graph
		if (this.showImportRates && this.canShowImport) {
			Rate[] tomorrowImport = this.rates.getTomorrowImport();
			this.canShowTomorrow = tomorrowImport.length > 0;

			// are we trying to show tomorrow's rates? Can we?
			if (!this.showToday && this.canShowTomorrow) {
				// yes - show tomorrow's rates
				displayRates = tomorrowImport;
				title = "Tomorrow's import rates.";
			} else if (todayImport.length > 0) {
				// no - show today's rates
				this.showToday = true;
				displayRates = todayImport;
				title = "Today's import rates.";
			} else {
				// no import rates
			}
		} else if (!this.showImportRates && this.canShowExport) {
			// export rates
			Rate[] tomorrowExport = this.rates.getTomorrowExport();
			this.canShowTomorrow = tomorrowExport.length > 0;

			// are we trying to show tomorrow's rates? Can we?
			if (!this.showToday && this.canShowTomorrow) {
				// yes - show tomorrow's rates
				displayRates = tomorrowExport;
				title = "Tomorrow's export rates.";
			} else if (todayExport.length > 0) {
				// no - show today's rates
				this.showToday = true;
				displayRates = todayExport;
				title = "Today's export rates.";
			} else {
				// no export rates
			}
		}

		// try to highlight the rate selected by the user
		if (this.selectedRateStartMoment != null && displayRates != null) {
			// explicitly selecting a rate
			for (int i = 0; i < displayRates.length; i++) {
				Rate rate = displayRates[i];
				if (rate.validFrom == selectedRateStartMoment) {
					highlightedRate = rate;
					title += " " + rate.valueIncVat.toString() + "p between " + hourMinuteDateFormat.format(rate.validFrom) + " and " + hourMinuteDateFormat.format(rate.validTo) + ".";
				}
			}
		}

		// fallback to highlighting the current rate
		if (highlightedRate == null && displayRates != null) {
			// selecting the default rate, which is the current rate (if it exists)
			Date now = new Date();
			for (int i = 0; i < displayRates.length; i++) {
				Rate rate = displayRates[i];
				if (rate.validFrom.compareTo(now) <= 0 &&
					rate.validTo.compareTo(now) > 0) {
						highlightedRate = rate;
						title += " Currently " + rate.valueIncVat.toString() + "p between " + hourMinuteDateFormat.format(rate.validFrom) + " and " + hourMinuteDateFormat.format(rate.validTo) + ".";
				}
			}
		}

		this.displayRates = displayRates;
		this.highlightedRate = highlightedRate;
		this.title = title;

		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
				OctopusView.this.doUpdateUI(rebuild);
			}
		});
	}

	private void doUpdateUI(boolean rebuild) {
		this.octopusGraph.setRates(this.displayRates, this.highlightedRate);
		this.titleView.setText(this.title);
		this.octopusButtons.setState(this.canShowTomorrow, this.canShowImport, this.canShowExport);

		if (rebuild) {
			this.doRebuild();
		}

		this.screenRefresh.doRefresh();
	}

	private void startSelectedRateResetTimer() {
		if (this.selectedRateResetTimer != null) {
			this.selectedRateResetTimer.cancel();
		}
		this.selectedRateResetTimer = null;

		if (this.selectedRateStartMoment != null) {
			this.selectedRateResetTimer = new Timer();
				this.selectedRateResetTimer.schedule(new TimerTask() {
					public void run() {
						OctopusView.this.selectRate(null);
					}
				}, 10000);
		}
	}

	private void stopAutomaticRateSelectionTimer() {
		if (this.automaticRateSelectionTimer != null) {
			this.automaticRateSelectionTimer.cancel();
			this.automaticRateSelectionTimer = null;
		}
	}

	private void startAutomaticRateSelectionTimer() {
		this.stopAutomaticRateSelectionTimer();

		if (this.displayRates == null) {
			return;
		}

		Rate currentRate = null;
		Date now = new Date();

		for (int i = 0; i < this.displayRates.length; i++) {
			Rate rate = this.displayRates[i];
			if (rate.validFrom.compareTo(now) <= 0 &&
				rate.validTo.compareTo(now) > 0) {
					currentRate = rate;
			}
		}

		if (currentRate != null) {
			// repaint when the current rate becomes invalid
			this.automaticRateSelectionTimer = new Timer();
			this.automaticRateSelectionTimer.schedule(new TimerTask() {
				public void run() {
					// maybe start requesting new rates
					OctopusView.this.rates.start();

					// update the UI
					OctopusView.this.updateUI(false);

					// and start the next timer
					OctopusView.this.startAutomaticRateSelectionTimer();
				}
			}, currentRate.validTo);
		}
	}

	public void start() {
		this.rates.start();

		this.startAutomaticRateSelectionTimer();
		this.updateUI(true);
	}
	
	public void stop() {
		this.rates.stop();
		this.stopAutomaticRateSelectionTimer();
	}

	public void destroy() {
		if (this.wiFiConnectedWhenLaunched && !KindleWiFi.SHARED.isConnected()) {
			KindleWiFi.SHARED.turnOn();
		}
	}

	public JComponent view() {
		return this.panel;
	}

	// setting the main states

	private void setRequesting() {
		if (this.displayRates != null) {
			return;
		}

		this.requesting = true;
		this.error = null;

		this.updateUI(true);
	}

	private void handleNewRates() {
		boolean rebuild = this.displayRates == null || requesting || error != null;

		this.requesting = false;
		this.error = null;

		this.updateUI(rebuild);
		
		this.startAutomaticRateSelectionTimer();
	}

	private void setError(String error) {
		boolean rebuild = this.displayRates != null || requesting || this.error != error;

		this.displayRates = null;
		this.requesting = false;
		this.error = error;

		this.updateUI(rebuild);
	}

	private void doRebuild() {
		this.panel.removeAll();

		if (this.displayRates != null) {
			this.panel.add(this.titleView, BorderLayout.PAGE_START);
			this.panel.add(this.octopusGraph, BorderLayout.CENTER);
			this.panel.add(this.octopusButtons, BorderLayout.PAGE_END);
		} else if (this.requesting) {
			this.panel.add(new JLabel("requesting..."), BorderLayout.CENTER);
		} else if (this.error != null) {
			this.panel.add(new JLabel(this.error), BorderLayout.CENTER);
			// TODO: retry button
		}
	}
}
