package uk.co.greenlightapps.info.octopus;

import uk.co.greenlightapps.info.util.AsynchronousHTTPRequest;

public class Account {
    public interface IAccountFetchListener {
        void onSuccess(Account account);
        void onFailure(Exception e);
    }

    private Account() {

    }

    public static void get(String accountNumber, String apiKey, IAccountFetchListener callback) {
        AsynchronousHTTPRequest.getInstance().addRequest("https://api.octopus.energy/v1/accounts/".concat(accountNumber), new AsynchronousHTTPRequest.IAsynchronousHTTPResponse() {
            public void onSuccess(String body) {
                System.out.println("-> ".concat(body));
            }

            public void onFailure(Exception e) {
                System.out.println("-> failure ".concat(e.toString()));
            }
        });
    }
}
