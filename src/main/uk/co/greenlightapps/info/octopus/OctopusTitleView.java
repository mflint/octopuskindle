package uk.co.greenlightapps.info.octopus;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class OctopusTitleView extends JPanel {
	private JLabel title;

	public OctopusTitleView() {
		this.title = new JLabel();
		this.add(this.title);
	}

	public void setText(String text) {
		this.title.setText(text);
	}
}
