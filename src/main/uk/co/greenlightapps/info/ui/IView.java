package uk.co.greenlightapps.info.ui;

import javax.swing.JComponent;

public interface IView {
	void start();
	void stop();
	void destroy();
	
	JComponent view();
}
