package uk.co.greenlightapps.info.ui;

import java.awt.FontMetrics;
import java.awt.Graphics;

public class GraphicsDrawString {
	public static class Anchor {
		public static final Anchor TRAILING = new Anchor();
		public static final Anchor TOP = new Anchor();

		private Anchor() {}
	}

	private static int H_PADDING = 8;

	private Graphics g;
	private FontMetrics fontMetrics;
	private int leading;
	private int ascent;
	private int descent;

	public GraphicsDrawString(Graphics g) {
		this.g = g;
		this.fontMetrics = g.getFontMetrics();
		this.leading = this.fontMetrics.getLeading();
		this.ascent = this.fontMetrics.getAscent();
		this.descent = this.fontMetrics.getDescent();
	}

	public void drawString(String string, int x, int y, Anchor anchor) {
		// see https://stackoverflow.com/a/1055884/408142
		int width = this.fontMetrics.stringWidth(string);

		if (anchor == Anchor.TRAILING) {
			x -= width + H_PADDING;
			y += (this.ascent / 2);
		} else if (anchor == Anchor.TOP) {
			x -= (width / 2);
			y += this.leading + this.ascent + this.descent;
		}

		this.g.drawString(string, x, y);
	}
}
