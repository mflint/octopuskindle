package uk.co.greenlightapps.info.ui;

import java.awt.Color;

public class Greyscale {
	// index 0: black
	// index 15: white
	// index 16: IndexOutOfBoundsException, haha
	public static Color[] value = new Color[16];

	static {
		for (int i = 0; i < value.length; i++) {
			int c = (int)((float)i * 17);
			value[i] = new Color(c, c, c);
		}
	}

	private Greyscale() {}
}
