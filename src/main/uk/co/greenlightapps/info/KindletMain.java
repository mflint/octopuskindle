package uk.co.greenlightapps.info;

import com.amazon.kindle.kindlet.AbstractKindlet;
import com.amazon.kindle.kindlet.KindletContext;

import uk.co.greenlightapps.info.util.KindleScreenSaver;

public final class KindletMain extends AbstractKindlet {
	private KindleScreenSaver screenSaver;
	private Info info;

	/**
	 * Invoked when the application is created. This method will be invoked
	 * exactly once during the application's life cycle. It is safe to create
	 * and add user interface components from this method since the user
	 * interface has not been realized yet. This method is restricted to five
	 * seconds of execution time. Expensive operations should be done
	 * asynchronously. The instantiation of the class (including the constructor
	 * and any static initializers) counts against this five seconds.
	 */
	public void create(KindletContext kindletContext) {
		this.info = new Info();
		this.info.init(kindletContext);
	}

	/**
	 * Invoked when the application is about to move to the foreground from a
	 * stopped state. This method may be invoked repeatedly during an
	 * application's life. The application's root container is made visible
	 * after this method executes. This method is restricted to five seconds of
	 * execution time. Expensive operations should be done asynchronously.
	 */
	public void start() {
		// this makes sure that the screensaver is disabled
		this.screenSaver = new KindleScreenSaver();
		this.info.start();
	}

	/**
	 * Invoked when the application is about to move to the background from a
	 * running state. This method may be invoked repeatedly during an
	 * application's life. The application's root container is made invisible
	 * after this method executes. Applications must close file handles when
	 * stop is called since the file system may become inaccessible while the
	 * application is stopped. This method is restricted to five seconds of
	 * execution time. Expensive operations should be done asynchronously.
	 */
	public void stop() {
		// if the screensaver was enabled when the kindlet launched,
		// this will re-enable it
		this.screenSaver.restore();
		this.info.stop();
	}

	/**
	 * Invoked when the application will not longer be needed. This method will
	 * only be invoked once during an application's life. This method is
	 * restricted to five seconds of execution time. Expensive operations should
	 * be done asynchronously.
	 */
	public void destroy() {
		this.info.destroy();
	}
}
