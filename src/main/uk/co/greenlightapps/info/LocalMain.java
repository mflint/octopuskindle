package uk.co.greenlightapps.info;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.input.GlobalGestureHandler;
import com.amazon.kindle.kindlet.input.keyboard.OnscreenKeyboardManager;
import com.amazon.kindle.kindlet.net.Connectivity;
import com.amazon.kindle.kindlet.security.SecureStorage;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.OrientationController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.io.File;
import javax.swing.JFrame;

public final class LocalMain extends JFrame {
	private Info info;

	private LocalMain() {
		this.info = new Info();
		FakeKindleContext context = new FakeKindleContext();
		this.info.init(context);

		this.setSize(new Dimension(800, 600));
		this.setContentPane(context.getRootContainer());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.start();
	}

	public static void main(String[]args) {
		LocalMain frame = new LocalMain();
		frame.setVisible(true);
	}

	public void start() {
		this.info.start();
	}

	public void stop() {
		this.info.stop();
	}

	public void destroy() {
		this.info.destroy();
	}

	private static final class FakeKindleContext implements KindletContext {
		private final Container rootContainer = new Panel(new BorderLayout());
		private final FakeKindleContext.FakeOrientationController fakeOrientationController = new FakeKindleContext.FakeOrientationController();
		private final FakeKindleContext.FakeResources fakeResources = new FakeKindleContext.FakeResources();

		public FakeKindleContext() {
		}

		public Connectivity getConnectivity() {
			return null;
		}

		public GlobalGestureHandler getGlobalGestureHandler() {
			return null;
		}

		public File getHomeDirectory() {
			return null;
		}

		public OnscreenKeyboardManager getOnscreenKeyboardManager() {
			return null;
		}

		public Container getRootContainer() {
			return this.rootContainer;
		}

		public SecureStorage getSecureStorage() {
			return null;
		}

		public Object getService(Class type) {
			if (type == OrientationController.class) {
				return this.fakeOrientationController;
			}
			
			if (type == KindletUIResources.class) {
				return this.fakeResources;
			}
			
			return null;
		}

		public KindletUIResources getUIResources() {
			return this.fakeResources;
		}

		public void setMenu(KMenu var1) {
		}

		public void setSubTitle(String var1) {
		}

		private static final class FakeResources extends KindletUIResources {
			private FakeResources() {
			}

			public Color getBackgroundColor(KColorName var1) {
			return null;
			}

			public Color getBackgroundColor(String var1) {
			return null;
			}

			public Font getBodyFont(KFontFamilyName var1) {
			return null;
			}

			public Font getBodyFont(KFontFamilyName var1, KFontStyle var2) {
			return null;
			}

			public Color getColor(KColorName var1) {
			return null;
			}

			public Color getColor(String var1) {
			return null;
			}

			public Font getFont(KFontFamilyName var1, int var2) {
			if (KFontFamilyName.MONOSPACE == var1) {
				return new Font("Monospaced", 0, var2);
			} else if (KFontFamilyName.SANS_SERIF == var1) {
				return new Font("SansSerif", 0, var2);
			} else {
				return KFontFamilyName.SERIF == var1 ? new Font("Serif", 0, var2) : null;
			}
			}

			public Font getFont(KFontFamilyName var1, int var2, KFontStyle var3) {
			return null;
			}
		}

		private static final class FakeOrientationController implements OrientationController {
			private FakeOrientationController() {
			}

			public void unlockOrientation() {
			}

			public void lockOrientation(int var1) {
			}

			public boolean isOrientationLocked() {
			return false;
			}

			public int getSensorOrientation() {
			return 0;
			}

			public int getOrientation() {
			return 0;
			}
		}
	}
}
